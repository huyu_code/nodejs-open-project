const crypto = require('crypto');
const { salt } = require('../config')

// md5加密数据
const encryptionData = (str) => {
    const newStr = str + salt
    const md5 = crypto.createHash('md5');
    md5.update(newStr);
    return md5.update(newStr).digest('hex')
}

module.exports = {
    encryptionData
}