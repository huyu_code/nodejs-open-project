const express = require('express');
const router = express.Router();
const { Blog } = require('../models/blog');
const User = require('../models/user');

router.get('/', async function(req, res, next) {
  try {
      const blogs = await Blog.find().sort({"create_date": -1}).populate('tag').limit(10).lean(); // 获取10条数据
      res.render('index.html', {blogs});
  } catch (e) {
      console.log(e)
      res.render('index.html')
  }
});

module.exports = router;
