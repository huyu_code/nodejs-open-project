import Vue from 'vue'
import Vuex from 'vuex'

// 假定我们有一个可以返回 Promise 的
// 通用 API（请忽略此 API 具体实现细节）
import { getBookInfo } from '../api/index'

Vue.use(Vuex)

export function createStore () {
  return new Vuex.Store({
    // 全局的数据，相当于vue data
    state: {
      items: {}
    },
    // 异步
    actions: {
      reqBookInfo ({ commit }) {
        // `store.dispatch()` 会返回 Promise，
        // 以便我们能够知道数据在何时更新
        return getBookInfo().then(res => {
          commit('setItems', res.data)
        })
      }
    },
    mutations: {
      setItems (state, items) {
        state.items = items
      }
    }
  })
}
