// 服务端入口
// 创建vue实例

import Vue from 'vue'
// 注意一定要使用App.vue而非App，不然会与app.js冲突
import App from './App.vue'
import createRouter from './router/index2'
import { createStore } from './store/store'
import { sync } from 'vuex-router-sync'

export default function createApp () {
  const router = createRouter()
  const store = createStore()

  // 同步路由状态(route state)到 store
  sync(store, router)

  const app = new Vue({
    router,
    store,
    render: h => h(App)
  })
  return { app, router, store }
}
