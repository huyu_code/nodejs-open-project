# node.js开放项目

#### 介绍
express 基础项目，用于了解和实践

#### 软件架构
包含首页、登录、注册、个人中心、文章详情、发布文章页面<br>
值得注意的地方<br>
1.文件上传<br>
使用三方中间件multer<br>
```shell script
npm install multer --save
```
文档地址：https://github.com/expressjs/multer/blob/master/doc/README-zh-cn.md<br>
```javascript
var multer = require('multer');  // 文件中间件
var storage = multer.diskStorage({ // 自定义磁盘存储
    destination: function (req, file, cb) {
        //注意 ./public/uploads，就是myapp/public/uploads
        cb(null, './public/uploads'); // 存储位置
    },
    filename: function (req, file, cb) {
        var [name, suffix] = file.originalname.split('.');
        cb(null, `${name}_${new Date().getTime()}.${suffix}`);
    }
});
var upload = multer({storage: storage});
// 配置upload后会自动保存上传文件
router.post('/center', upload.single('avatar'), async function (req, res, next) {});
```
2.session持久化<br>
借助三个包<br>
```shell script
npm install express-session --save
npm install redis --save
npm install connect-redis --save
```
```javascript
var session = require('express-session');
// redis持久化session
const redis = require('redis');
var RedisStore = require('connect-redis')(session);
let redisClient = redis.createClient();

app.use(session({  // 处理session
  secret: 'ddf8834/+-d*/f8f4dsqe21ere55$dd26333998', // 加密
  store:new RedisStore({ client: redisClient }), // 将session保存到redis
  cookie: {
    maxAge: 24 * 60 * 60 * 1000 // 1天
  },
  resave: false, //指每次请求都重新设置session cookie，cookie是10分钟过期，每次请求都会再设置10分钟
  saveUninitialized: true // 无论你是否使用session，我都给你分配一把钥匙，
  // 即当客户端没有session_id时每次都会在创建这个cookie
}));
```
3.自定义过滤器<br>
使用的是art-template<br>
```shell script
template.defaults.imports.dateFormat = function(date, format){
  moment.locale('zh-cn');
  return moment(date).format(format);
};
```