const Vue = require('vue')
const render = require('vue-server-renderer').createRenderer()
const app = require('express')()

const page = new Vue({
  template: '<div>666</div>'
})

app.get('/', async (req, res) => {
  try {
    // 将vue实例转为html
    const html = await render.renderToStream(page)
    app.end(html)
  } catch (e) {
    app.status(500).end('服务端异常')
  }
})

app.listen(8000, () => {
  console.log('listen 8000')
})
