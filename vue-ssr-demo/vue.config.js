// 安装依赖
// cnpm install webpack-node-externals lodash.merge -dev
const VueSSRServerPlugin = require('vue-server-renderer/server-plugin') // 生成服务端的bundle
const VueSSRClientPlugin = require('vue-server-renderer/client-plugin') // 生成客户端的bundle
const nodeExternals = require('webpack-node-externals')
const merge = require('lodash.merge')

// 环境变量，决定入口是客户端还是服务端
const TARGET_NODE = process.env.WEBPACK_TARGET === 'node'
const target = TARGET_NODE ? 'server' : 'client'

module.exports = {
  css: {
    extract: false
  },
  outputDir: './dist/' + target,
  configureWebpack: () => ({
    // 将entry指向应用程序 server/client文件
    entry: `./src/entry-${target}.js`,
    // 对bundle renderer提供source map 支持
    devtool: 'source-map',
    // 这允许webpack以Node适用的方式处理动态导入(dynamic import)
    // 且还会编译Vue 组件时告知 `vue-loader` 输送面向服务器的代码(server-orientedcode)
    target: TARGET_NODE ? 'node' : 'web',
    node: TARGET_NODE ? undefined : false,
    output: {
      // 此处告知server bundle 使用Node风格导出模块
      libraryTarget: TARGET_NODE ? 'commonjs2' : undefined
    },
    externals: TARGET_NODE ? nodeExternals({
      // 不要外置化 webpack 需要处理的依赖模块
      // 可以在这里添加更多的文件类型，如，未处理的*.vue原始文件
      // 你应该将修改 global (如polyfill)的依赖模块列入白名单
      allowlist: [/\.css$/]
    }) : undefined,
    // 这里将服务器的整个输出构建为单个JSON文件插件
    // 服务端默认文件名为 vue-ssr-server-bundle.json
    plugins: [TARGET_NODE ? new VueSSRServerPlugin() : new VueSSRClientPlugin()]
  }),
  chainWebpack: config => {
    config.optimization.splitChunks(undefined)
    config.module.rule('vue').use('vue-loader').tap(options => {
      merge(options, {
        optimizeSSR: false
      })
    })
  }
}
