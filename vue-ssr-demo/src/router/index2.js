import Vue from 'vue'
import VueRouter from 'vue-router'
import Detail from '@/views/Detail'
import Index from '@/views/Index'

Vue.use(VueRouter)

// 为每个请求创建一个新的根 Vue 实例。
// 这与每个用户在自己的浏览器中使用新应用程序的实例类似。
// 如果我们在多个请求之间使用一个共享的实例，很容易导致交叉请求状态污染 (cross-request state pollution)。
export default function createRouter () {
  return new VueRouter({
    mode: 'history',
    routes: [
      { path: '/', component: Index },
      { path: '/detail', component: Detail }
    ]
  })
}
