var express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
var Schema = mongoose.Schema; // 模式，限制mongodb文档数据结构

// 连接数据库
// 1.指定连接的数据库不需要存在
mongoose.connect('mongodb://localhost/mongoose', {useUnifiedTopology: true});
// 2.设计表结构
var userSchema = new Schema({
    username: {
        type: String,
        required: true  // 必须的
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String
    }
});

// 3.将文档结构发布为模型
// mongoose.model方法就是用来将一个架构发布为model
// 第一个参数:传入一个大写名称单数字符串用来表示你的数据库名称
//        mongoose会自动将大写名词的字符串生成小写复数的集合名称
// 第二个参数: 架构Schema
// 返回值：模型构造函数
var User = mongoose.model('User', userSchema);

// 4.当有了模型构造函数后，可以使用这个构造函数对users集合进行操作
var user = new User({
    username: 'huyu',
    password: '123456',
    email: 'aaa@qq.com'
});

// 保存,这些方法都返回一个Promise对象
user.save(function (err, ret) {
    if (err) {
        console.log('保存失败');
    } else {
        console.log('保存成功');
        console.log(ret);
    }
});
// 查询
User.find(function (error, ret) {
    if (err) {
        console.log('保存失败');
    } else {
        console.log('查询成功');
        console.log(ret);
    }
});
// 条件查询
User.find({
    username: 'huyu'
}, function (err, ret) {
    if (err) {
        console.log('保存失败');
    } else {
        console.log('查询成功');
        console.log(ret);
    }
});
// 更新数据
User.findOneAndUpdate({
    username: 'huyu'
}, {
    email: 'xxx@163.com'
}, function (error, ret) {
    if (error) {
        console.log('更新失败');
    } else {
        console.log('更新成功');
    }
});

// 删除数据
User.deleteOne({
    username: 'huyu'
}, function (error, ret) {
    if (error) {
        console.log('删除失败');
    } else {
        console.log('删除成功')
    }
});