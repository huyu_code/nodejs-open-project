var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/blog', {useNewUrlParser: true, useUnifiedTopology: true});
var Schema = mongoose.Schema;

var userSchema = new Schema({
    email: {
        type: String,
        required: true
    },
    username: { // 用户名
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true,
        select: false
    },
    created_time: {
        type: Date,
        default: Date.now
    },
    last_modified_time: {
        type: Date,
        default: Date.now,
        select: false
    },
    avatar: {
        type: String,
        default: '/images/user_normal.jpg'
    },
});

module.exports = mongoose.model('User', userSchema);
