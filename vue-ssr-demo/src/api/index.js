import axios from 'axios'

export const getBookInfo = () => {
  return new Promise((resolve, reject) => {
    axios.get('http://39.101.163.227:8000/books/infos').then(res => {
      resolve(res)
    }).catch(e => reject(e))
  })
}
