const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/blog', {useNewUrlParser: true, useUnifiedTopology: true});
const Schema = mongoose.Schema;

const blogTagSchema = new Schema({
    blog_tag: { // 博客标签
        type: String,
        required: true
    },
    create_date: { // 创建时间
        type: Date,
        default: Date.now
    },
})

const blogSchema = new Schema({
    title: { // 标题
        type: String,
        required: true
    },
    tag: { // 博客标签
        type: String,
        required: true,
        ref: 'BlogTag'
    },
    content: { // 博客内容
        type: String,
        required: true
    },
    user: { // 作者
        type: String,
        ref: require('./user'),
        required: true
    },
    cover: { // 封面图
        type: String,
    },
    read_num: { // 浏览量
        type: Number,
        required: true,
        default: 0
    },
    create_date: { // 创建时间
        type: Date,
        default: Date.now
    },
    edit_date: {
        type: Date,
        default: Date.now,
        select: false
    },
    __v: {type: Number, select: false},
})
exports.BlogTag = mongoose.model('BlogTag', blogTagSchema);
exports.Blog = mongoose.model('Blog', blogSchema);
